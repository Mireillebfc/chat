import React, { Component } from 'react';
import io from 'socket.io-client';

class App extends Component {

  state = {
    isConnected: false,
    id: null,
    peeps: [],
    message: "",
    old_messages:[]
  }
  socket = null

  componentDidMount() {

    this.socket = io('http://codicoda.com:8000');

    this.socket.on('connect', () => {
      this.setState({ isConnected: true })
    })


    this.socket.on('youare', (answer) => {
      this.setState({ id: answer.id })
    })

    this.socket.on('peeps', (answer) => {
      this.setState({ peeps: answer })
    })

    this.socket.on('disconnect', () => {
      this.setState({ isConnected: false })
    })


    this.socket.on('room', old_messages => this.setState({old_messages}))

      /* Fifth Msg
    Congrats! You can send and receive chat messages. Now: 
     1. display the old messages (you're receiving them when listening to `room`). Just take this array and put it in state and render it.
     2. display the new message (add it to the array) 
     3. make the id auto-loaded, so you don't have to click on the button evey time to obtain an id. 
    4. make it look the best you can! */



  }

  componentWillUnmount() {
    this.socket.close()
    this.socket = null
  }

  send = (text) =>{
    const spam = {
      id: this.state.id,
      name: "Mimita",
      text
    }
    this.socket.emit("message", spam);

    let old_messages = this.state.old_messages;
    old_messages = [...old_messages,spam]
    this.setState({old_messages})
    
  }


  render() {


    return (
      <div className="App">
        <div>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
        <div>id: {this.state.id}</div>

        <button onClick={() => this.socket.emit('whoami')}>Who am I?</button>
        {/*Bravo! Now please emit "give me next" from the client. You
        can do that on start, or when pressing a button, whatever */}

        {/* Impressive. We're going to top it up a notch. 
        The server will send you a simple, random, addition.You will need to answer correctly.
        You cannot put the answer in a button, because if you do, the page will refresh, and you will lose your ID. 
        Try to solve it! The API to get an equation is to send "addition" */}

        {/* { /*<button onClick={() => this.socket.emit('addition')}>addition</button> */}


        {/* please send the message "answer" with the answer to 35 +62+ 32. 
        You may send the answer as a string. You may also get a hint.*/}

        <input type="text" onChange={e => this.setState({ message: e.target.value })} />
        <button onClick={() => this.send(this.state.message)}>send</button>

        {/*Nice! You solved it. You can now send arbitrary things to the server. That's cool. Let's take a moment to clean up. We're going to keep:
      1. the "whoami" button
      2. the input that sends text
      3. the button that sends that text
You can also keep the labels that say if you are connected or not. Remove everything else.
What you'll also do is, instead of "answer", make the button submit to an endpoint called "message".
You can send any piece of text there. See what happens.
Do clean first though!*/
        }
        <p>{this.state.peeps.length} peeps are online</p>
        {this.state.peeps.map(peep =>
          <div key={peep}>
            <p>{peep}</p>
          </div>
        )}

        {this.state.old_messages.map(msg => 
        <div>
            <h1>{msg.name}</h1>
            <p>{msg.text}</p>
            <p> {msg.date}</p>

        </div>)
        }
      </div>
    )
  }

}



export default App;

